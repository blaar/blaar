#Basic Librairies And Applications for Robotics (BLAAR)
#Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2015 - 2017)
#This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
#You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
#In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
#Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
#The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

if [ $# -ne 1 ]
then
echo "You need a project directory as argument."
echo "Usage : xcode_project <project_directory>"
exit 1
fi

#We go to the project directory and get the directory name then we go back to seach for the baar directory
cd $1
project_dir="$PWD"
project_name=$(basename $project_dir)
cd - >/dev/null #Now we have the project directory we go back in current directory without message (/dev/null)

#We go in blaar directory
cd $(dirname $0)
blaar_dir="$PWD"
blaar_build_dir="${blaar_dir}_build"




developer_tools/create_project.sh $1 $blaar_build_dir xcode
open $blaar_build_dir/cmake_files_xcode/$project_name/$project_name.xcodeproj
