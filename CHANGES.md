2017 July 29 Arnaud Blanchard

Applications
------------

* blc_channels: remove unused channel has been fixed
* o_gtk_image [FL32,Y800] add min/max option to scale the pixel values
* o_gtk_image display the name of the channel in the window's tilte
* o_gnuplot fix a problem using history with synchronized channel. There is still a small problem while display the time scale
* New script **e_profile.sh** which displays the graphs of the profiling of differents apps: i.e. 'e_profile.sh image.tsv gradient.tsv display.tsv' ( see profile option of blc_program)
* Add documentation (`PROFILING.md`) axplaining how to use the new profile option.

Libraries
---------

* blc_channels fix synchronisation problems
* blc_program in you can use command with parameter in one line (i.e. in blc_channels i3 display image of channel 3)
* blc_program add option --profile<filename.tsv> (-~) which record the stat of the execution loop. It can then be displayed with **e_profile.sh**
