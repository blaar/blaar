You can copy past each example in your terminal. Press 'q' to quit the test. For each function you can see the list of options with `-h`

Sound
=====

Raw signal
----------

**Display the graph of the microphone**

    e_graph_sound.sh

![image](http://blaar.org/images/examples/graph_microphone.png)

The time is 92ms because it is size/samplerate i.e. 4096/44100

**Record the microphone in a sound file**

    i_sound.sh | o_sound.sh -f toto.wav

**Replay the sound on the speakers**

    i_sound.sh -f toto.wav | o_sound.sh

**Show the graph of the file**

    e_graph_sound.sh -f record.wav

**Graph a pure sinusoĩd at 440Hz (La)**

    e_graph_sound.sh -F 440

![image](http://blaar.org/images/examples/graph_440Hz.png)

* -F is the frequency used to generate the sinusoïd

**Record the sound in a .tsv (tab separated values) file.**
 
    i_sound.sh -o toto.tsv
    
**Play the 'La' in the speaker**

    i_sound.sh -F 440 | o_sound.sh 

Spectrum (fft)
--------------

**Apply a fft on sound to get the spectrum and display the result in a graph**

    e_graph_spectrum.sh

**Record the spectrum (fft) in a .tsv file ( tab separated values reading by excel, gnuplot , ...)**

    i_spectrum.sh -o toto.tsv

**Apply a fft on a pure sinusoĩd signal of 440 and graph it**

    e_graph_spectrum.sh -F440 -X4000
    
![image](http://blaar.org/images/examples/spectrum_440Hz.png)

* -X represents the maximum frequency we want on the graph

Give the max of the frequency from the microphone
-------------------------------------------------

    i_spectrum.sh | f_max --find_arg=10.77 --output=-

* --find_arg means we do not want the value of max but the index of the max. 10.77 means we scale the index by 44100/4096 to convert index in Hz.
* --output select where we want to put the output. '-' means it is not a channel but the terminal.

Give the max of the 440Hz sinusoĩd
----------------------------------

    i_spectrum.sh -F440 | f_max --find_arg=10.77 --output=-

This return: 446.955017 on the terminal

The imprecision come from the discretisation of the fourier transform. If you take more items per buffer the precision is much better. i.e.:

    i_spectrum.sh -F440 -s32000 | f_max --find_arg=1.378 --output=-

* -s32000 means that we take a buffer of 32000 floats instead of the default 4096
* --find_arg=1.378 because 44100/32000 = 1.378

This return a value of 440.270996
The refresh rate is now only of 0.7s (32000/44100) before it was 93ms. We can play with the samplerate (-S) to improve it.



