
Installing
==========

Some projects like the libraries need to be installed. In this case you can do **`./install <project> [<Release|Debug|RelWithDebugInfo|RelMinSize>]`**

This will compile and install the project. You computer password may be requested if the install needs to access to `/usr/local`or similar. It is the case for the basic libraries. It is safe to use './install.sh' for all projects even those which does not need install. Using **`./run.sh <project directory`** (see compiling below) may be simpler.

If you want install all the cloned submodules, use **`./install.sh all [<Release|Debug|RelWithDebugInfo|RelMinSize>]`**. If your project is not a cloned submodule it will not be installed by `all`. Precise your exact project directory. 

Compiling
=========

Use `./compile.sh <project directory> [Release|Debug|RelWithDebugInfo|RelMinSize]` (blibs/blc_core, blgtk, ...)

The binaries will be in `build/<Release|Debug|RelWithDebugInfo|RelMinSize>`and the libraries in the subdirectory `lib`. The cmake builds will be in  `build/cmake_files_<Release|Debug|RelWithDebugInfo|RelMinSize|eclipse|xcode>/`. Remove this directory to solve cmake problems during compilation.

`build/` is a symbolic link toward a real directory which is outside your current directory usually :`../blaar_build`. The prefix automatically takes the value of your directory (i.e. if you are in `test_blaar/` the real build directory will be `../test_blaar_build/`. This allows you to have different versions of blaar in different directories (we recommend to use git branches instead of different copies). 

When your are coding on your project you should use **`./run.sh <project directory>`** instead of directly calling your binary. Like this, you are sure to use the last compilation of your code. `./run.sh <project directory>` automatically compiles your code before executing it.

Debugging
=========

In case of problem you may use a debugger: **`./debug.sh <project directory>`** or realize memory checks with valgrind: **`./valgrind.sh <project directory>`**. The project is automatically recompiled in debug mode and the appropriate debugger (gdb, lldb, valgrind) is called.

If the problem appear in a library you may reinstall all the projects in Debug mode:  **` ./install.sh all Debug`**

Eclipse
=======

`./eclipse_create_project.sh <project dir>`
	
Follow the instruction to import the project in eclipse

If you want to debug in eclipse : **debug->debug configurations->new C/C++ application->browse: `<blaar_build>/Debug/<project>`**

Xcode (Mac OSX)
===============

`./xcode_project <project>`

**xcode** will be automatically launched with the project. 

If you want to debug: select an executable target instead of `ALL_BUILD` and press the play button. To precise arguments, click on the executable target then `Edit Echeme` then `Arguments`->`Argument Passed On Launch`. Similarly, you can ask the target to be executed in a specific directory going to `Options`->`Working Directory`.

Code documentation
==================

To see the code documentation: **`./doc_api.sh <project>`.**. Add  **-c** option if you want only want the C accessible. The documentation will be opened automatically with your default browser.

The documentation is built with doxygen in `build/<cpp_api|c_api>/<project>/html/index.html`.

