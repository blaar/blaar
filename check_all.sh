# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

echo
echo "Check all projects"
echo "================="
echo
mkdir -p /tmp/blaar
#We make sure we respect the order of compilation in .gitmodules
cat .gitmodules | sed -n 's/.*path = //p' | while read project_path
do
   project_name=$(basename $project_path)
type $project_path/check.sh &> /dev/null && { #Check existence
            echo "Check **$project_path**";
            $project_path/check.sh &> /tmp/blaar/check_$project_name.log ||
            {
                echo "### Fail checking: '$project_path' ###"
                cat /tmp/blaar/check_$project_name.log
                echo
                exit 1
            }
    }
done

