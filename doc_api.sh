# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

usage(){
echo "usage: doc_api.sh  [-c] [-h] <project dir>"
echo
echo "-c     document the plain c version (c++ by default)"
echo "-h     display this help"
echo
}

[ $# -lt 1 ] && { echo "Missing project directory."; usage; exit 1; }

target="cpp_api"

args=`getopt ch $*`
if [ $? != 0 ]; then
usage
exit 2
fi

set -- $args
while [ $# -gt 0 ]
do
case "$1" in
(-c) target="c_api";;
(-h) usage; exit 0;;
(--) shift; break;;
(-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
(*)  break;;
esac
shift
done

cd $1
project_dir="$PWD";
project_name=`basename $project_dir`;
cd - > /dev/null;

cd `dirname $0`
blaar_dir="$PWD"
blaar_build_dir="${blaar_dir}_build"
build_dir="$blaar_build_dir/$target/$project_name"

mkdir -p $build_dir

cd  $build_dir

#We prepare the doxygen config
echo > doxyfile.doxy
[ $target = "c_api" ] && {
echo "PROJECT_NAME              = \"$project_name API for C\"" >> doxyfile.doxy
echo "OPTIMIZE_OUTPUT_FOR_C     = YES"  >> doxyfile.doxy ;
} || {
echo "PROJECT_NAME              = \"$project_name API for C++\"" >> doxyfile.doxy
echo "PREDEFINED                = \"__cplusplus\"" >> doxyfile.doxy
}
echo "CASE_SENSE_NAMES          = YES"  >> doxyfile.doxy
echo "HTML_OUTPUT            	= html" >> doxyfile.doxy
echo "EXTRACT_LOCAL_CLASSES     = NO" >> doxyfile.doxy
echo "HIDE_UNDOC_MEMBERS        = YES" >> doxyfile.doxy
echo "GENERATE_LATEX            = NO" >> doxyfile.doxy
echo "INPUT                     = $project_dir" >> doxyfile.doxy
echo "MACRO_EXPANSION        	= YES" >> doxyfile.doxy
echo "QUIET                  	= YES" >> doxyfile.doxy
echo "RECURSIVE              	= YES" >> doxyfile.doxy
echo "SHOW_FILES			   	= NO" >> doxyfile.doxy
echo "WARN_IF_UNDOCUMENTED  	= NO" >> doxyfile.doxy
echo "JAVADOC_AUTOBRIEF         = YES" >> doxyfile.doxy
echo "PROJECT_LOGO              = $blaar_dir/logo_blaar.png" >> doxyfile.doxy
echo "WARNINGS                  = NO" >> doxyfile.doxy
echo "HTML_HEADER               = $blaar_dir/developer_tools/doc_header.html" >> doxyfile.doxy
echo "HTML_FOOTER               = $blaar_dir/developer_tools/doc_footer.html" >> doxyfile.doxy
echo "HTML_EXTRA_STYLESHEET     = $blaar_dir/developer_tools/doc.css" >> doxyfile.doxy

doxygen doxyfile.doxy

if [ $? ] ; then
if type xdg-open &>/dev/null; then xdg-open "$build_dir/html/index.html";
elif type open &>/dev/null; then open "$build_dir/html/index.html";
else echo "Open documentation in: $build_dir/html/index.html";
fi
else
echo "Failing building the documentation"
fi



