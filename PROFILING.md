Profiling
=========

Description
-----------

When you use many processes in queue it may be useful to study the sequence of execution and the average time spent for each.

To record this data you can use for each application the option `--profile=<filename.tsv>` (`-~<filename.tsv>`) The file will contain the loop starting time, the execution  starting time ( time once the data is ready) and the duration of the loop.

In first row loop starting time indicate in fact, the moment the application has started. The moment `blc_program_init` has been called.

    #start_time(?s) 	exe_time(?s)    	duration(?s)    	
    1501244087769618	               0	       0	
    1501244088341986	1501244088341998	     688	
    1501244088373970	1501244088373972	     169	
    1501244088409968	1501244088409969	     174	
    ...

To graph these data, you can use the **e_profile.sh <filename.tsv> [<filename.tsv> ...]**

This generates a graph showing the sequences of executions and a graph showing the average duration of the loop and the period of exectution of the loop.

Example
-------

    i_v4l2_camera -~camera.tsv | o_gtk_image -~display.tsv
    e_profile.sh camera.tsv display.tsv

This will profile the execution of image acquisition and display. Thne it will open two windows with the graph showing the sequence of all the executions and a graph showing the average time

