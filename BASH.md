Debugging
---------

Use bash option **-x** to show each bash execution

    bash -x my_script.sh
You will usually have to mush information. Active this option in your script only on the suspected bugging part.

    set -x #Active debbugging
    suspected_function
    set +x #Stopping debugging

You can also use **-v** which will display all the input (stdin) of the script

Finaly you can stop the bash to replace meta characters with **-f** e.g. `ls *` really looks for `*`

See more: [Bash for beginners](http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_02_03.html)