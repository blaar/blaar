Install blaar on Mac OSX
========================

[Video tutorial](http://www.etis.ensea.fr/neurocyber/blaar/videos/blaar_osx_install.mp4)

You need Xcode or a C/C++ compilation (clang, g++, ..). 

Copy past this line in a terminal

    curl -sS https://framagit.org/blaar/blaar/raw/master/developer_tools/clone_and_install_blaar_osx.sh | bash &&
    cd blaar

You can do [**step by step**](https://framagit.org/blaar/blaar/blob/master/developer_tools/clone_and_install_blaar_osx.sh) copying command line by command line. This may be useful to better understand and in case where the installer crash.

If you have a problem at compile time it may be due to a bad configuration of your compiler  [**see how to install command line tools mac os x**](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/)
    