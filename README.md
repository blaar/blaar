
[![logo](logo_blaar.png)](http://blaar.org)

Basic Libraries And Applications for Robotics
=============================================

BLAAR is still in active development. It is **not stable yet** and will evolve. Do no hesitate to [ask for improvements or notify issues](https://framagit.org/blaar/blaar/issues).

Basic Libraries for C/C++
-------------------------

- [blc_core](https://framagit.org/blibs/blc_core) : Generic helpers in C or C++ used by all other blaar projects
- [blc_channel](https://framagit.org/blibs/blc_channel) : Tools to use shared memory in synchrone ( in test) and asynchrone mode 
- [blc_image](https://framagit.org/blibs/blc_image) : Manipulate blc_array as images. Can load and save them as png files  
- [blc_program](https://framagit.org/blibs/blc_program) : Parse arguments and interacts with  user in commandline.

Standard blaar applications
---------------------------

- [bapps](https://framagit.org/blaar/bapps.git) : Generic executables to manipulate blc_channels (find max, generate oscillators, ...)

In `scripts/` you have bash scripts which combine applications to produce higher level functionalities. They may require you add optional modules.

Install
=======

* [Mac OSX](INSTALL_OSX.md)
* [Ubuntu](INSTALL_UBUNTU.md)

Adding submodules
-----------------

You will only have basic framework. You may add submodules as needed. To add modules use : `git submodule add <module repository>`. Once you have added a module, install it with **`./install.sh <module>|all`**

**all** install all your cloned modules at once.

Update the installation
-----------------------

**`./update_and_install_all.sh`**

This will download the new code and install it for blaar and each submodule

Usage 
=====

**`<binary> [--help] [args ...]`**

The availble blaar binaries are in `bin/` directory. All the binaries accept the **`--help`** option which shows a description of the project and details of possible others options. You can also use scripts which run a combinaison of binaries and other scripts. They are in `scripts/` directory and accept the **`-h`** option which shows a description of the project and details of possible others options.

When your are in development your are advised to see [developement](DEVELOPMENT.md).

You can see an [**example**](https://framagit.org/blibs/blc_core/wikis/home) of manipulating and displaying generic arrays with blc_core

You can create you first project with this example of [image manipulation](https://framagit.org/blibs/blc_image/wikis/home)

**SEE ALSO**  [examples](EXAMPLES.md) [development](DEVELOPMENT.md) [git](GIT.md) [Frequently Asked Questions](FAQ.md)

