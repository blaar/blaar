source blaar.sh

#check files exists
for file in "$@"
do
[ -e $file ] || { echo "Profile file '$file' not found"; exit 1; }
done

#generating time intervals
#cat <(  #select this to debug
gnuplot <(
echo "set term qt title 'Sequences $@'"
#find the initial time (i.e first time of the first file)
echo "stats '$1' every ::::0 using 1 nooutput"
echo "offset=STATS_min"
echo "set grid"
echo "set border 3"
echo "set lmargin at screen 0.1"
echo "set xlabel 'time (ms)' font ', 14'"
echo "set yrange[-$#:0.1]"
echo "set ytics font ', 14' scale 0 rotate by 45 right"
i=0
printf "set ytics (" #printf for no line return
for file in "$@"
do
printf "'$(basename $file .tsv)' -$i-0.1, "
let i++ || true #Warning do not use ((i++)) as it would be considered as error (i.e. !=0 ) and quit
done
echo ")"
printf "plot  "
i=0
for file in "$@"
do
printf "'$file' using ((\$1-offset)/1000):(-$#):(0):($#-$i) every ::0::0 notitle  with vector noborder  linewidth 1  linecolor $i, "
printf "'$file' using ((\$1-offset)/1000):(-$#):(0):($i) every ::0::0 notitle  with vector noborder linewidth 1  linecolor $i, "
printf "'$file' using ((\$1-offset)/1000):(-$i):(0):($i-$#) every ::1  notitle with vector noborder  nohead linewidth 1  linecolor $i, "
printf "'$file' using ((\$1-offset)/1000):(-$i-0.1):((\$2-\$1)/1000):(0) every ::1 notitle with vector noborder nohead  linewidth 1   linecolor $i , " # every ::1 avoid first point
printf "'$file' using ((\$2-offset)/1000):(-$i):(0):($i-$#) every ::1  notitle with vector noborder  nohead linewidth 1  linecolor $i, "
printf "'$file' using ((\$2-offset)/1000):(-$i-0.1):(\$3/1000):(0) every ::1 title '$(basename $file .tsv)' with vector noborder fixed linewidth 3 linecolor $i , " # every ::1 avoid first point
printf "'$file' using ((\$2-offset+\$3)/1000):(-$i):(0):($i-$#) every ::1  notitle with vector nohead linewidth 1  linecolor $i, "
let i++ || true
done
echo " -$# linewidth 2 notitle"
echo "pause mouse close" 
)&

#cat <(  #select this to debug
gnuplot <(
#generating duration stats for each files from command line
echo "set term qt title 'Stats $@'"
echo "set print 'durations.dat'"
for file in "$@"
do
echo "stats '$file' every ::1 using 3 nooutput" # every ::1 avoid first point
echo "print STATS_mean, STATS_stddev"
done
echo "set print"

echo "stats '$1' every ::::0 using 1 nooutput"
echo "offset=STATS_min"
echo "delta_v(x) = ( vD = x - old_v, old_v = x, vD)"
echo "old_v = offset"
echo "set xrange [1000000:*]" #We do stat before the first second 

echo "set print 'period.dat'"
for file in "$@"
do
echo "stat '$file' every ::1 using (\$1-offset):(delta_v(\$1)) nooutput"
echo "print STATS_mean_y, STATS_stddev_y"
done
echo "set print"

echo "stats 'period.dat' using 1 nooutput"
echo "period=STATS_max"

#graphic stats
echo "set multiplot layout 2, 1 "
echo "set title 'duration' font ',24'"
echo "unset key"
echo "set boxwidth 0.90"
echo "set border 3"
echo "set ylabel 'time (ms)' font ', 16'"
echo "set style fill transparent solid 0.5"
echo "set xrange[-0.5:$#-0.5]"
printf "set xtics (" #printf for no line return
i=0
for file in "$@"
do
printf "'$(basename $file .tsv)' $i, "
let i++ || true  #Warning do not use ((i++)) as it would be considered as error and quit
done
echo ") font ', 16'"
echo "plot 'durations.dat' using 0:(\$1/1000):(\$2/1000):0  with boxerror linecolor variable, '' using 0:(\$1/1000):(sprintf('%.3fms', \$1/1000)) with labels offset 0,1 font ',14'"
echo "set title  'period' font ',24'"
echo "unset key"
echo "plot 'period.dat'  using 0:(\$1/1000):(\$2/1000):0 with boxerror linecolor variable, '' using 0:(\$1/1000):(sprintf('%.3fms', \$1/1000)) with labels offset 0,1 font ',14'"
echo  "unset multiplot"
echo "pause -1"
)




