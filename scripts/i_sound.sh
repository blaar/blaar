# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

source blaar.sh

blaar_init_options "Acquire the sound from microphone or sound file (-f filename)"
blaar_add_option device D "" "device_name" "device to use for the microphone"
blaar_add_option filename f "" "filename" "file to load the sound from"
blaar_add_option frequency F "" "frequency" "frequency of a pure  sound"
blaar_add_option output o "" "blc_channel|-" "output on blc_channel or stdout(-)"
blaar_add_option size s "" "integer" "number of items per buffer" "4096"
blaar_add_option samplerate S "" "integer" "Samplerate of the sound acquisition" "44100"
blaar_parse_args "$@" 

[ $filename ] && file_option="--file=$filename" || file_option=""
[ $output ] && output_option="--output=$output"|| output_option=""

if [ $filename ]; then 
	echo "Loading '$filename'" >&2
	[ $frequency] &&{ echo "You cannot have file (-f) and frequecy (-F) at the same time"; exit 1; }
	[ $device ] && { echo "You cannot have file (-f) and microphone (-D) at the same time"; exit 1; }
	blaar_module sndfile i_sndfile --file=$filename --size=$size --samplerate=$samplerate $output_option
elif [ $frequency ]; then
	[ $device ] && { echo "You cannot have frequency (-F) and microphone (-D) at the same time"; exit 1; }
    echo "Generating sinusoïd of $size items at $frequency Hz" >&2
    i_oscillator --min=-1 --frequency=$frequency --size=$size --refresh=$(calc $size/$samplerate*1000) $output_option
elif [ $(uname) = "Darwin" ]; then #microphone
	echo "Coreaudio microphone sampling at $samplerate Hz in buffer of $size items" >&2
	[ $device ] && echo "On Darwin you cannot precise microphone (-D)"
	blaar_module coreaudio i_coreaudio --size=$size --samplerate=$samplerate $output_option
elif [ $(uname) = "Linux" ]; then
	echo "ALSA microphone sampling at $samplerate Hz in buffer of $size items" >&2
	[ ! $device ] && device=default
	blaar_module asound i_asound --device=$device --size=$size --samplerate=$samplerate $output_option
else 
	echo "$blaar_program_name does not work on '$(uname)' platform"
	exit 1
fi

