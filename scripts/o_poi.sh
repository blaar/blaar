source blaar.sh

blaar_init_options "Apply points of interest on ilage"
blaar_add_option max_nb n "" "interger" "number of max" "100"
blaar_add_option size G "" "(integer)x(interger)" "size of the filter" "8x8"
blaar_add_option size M "" "(integer)x(interger)" "size of the filter" "64x64"
blaar_add_option sigma S "" "real" "sigma of the filterr" -1
blaar_add_parameter image "blc_channel-in" "1" "image you want to apply the point of interest"
blaar_parse_args "$@"


f_corner.sh -G8x8 -M64x64 -o:corner$$ $image |
f_arg_max -n$max_nb -x36 -y36| { read arg_max
        o_gnuplot_poi $image $arg_max
}

blc_channels -u /corner$$
blc_channels -u /arg_max
