source blaar.sh

blaar_init_options "Conv image with  on black and white camera and display it"
blaar_parse_args "$@" 


i_d2gaussian --size=32x32 -o/kernel$$ && i_pngfile $1 --format=Y800 -o/image$$  
f_conv /image$$ /kernel$$ -o/conv$$ |o_gtk_image

#remove the used blc_channels
blc_channels --unlink /kernel$$ 
blc_channels --unlink /image$$ 
blc_channels --unlink /conv$$ 