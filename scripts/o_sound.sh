#!/usr/bin/env bash 

# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but not defined
set -o pipefail #Error if a pipe fail

source blaar.sh

blaar_init_options "Send data of sound to speaker or sound file (-f)"
blaar_add_option device D "" "device_name" "device to use for the speaker"
blaar_add_option filename f "" "filename" "file to save the sound to"
blaar_add_option size s "" "integer" "number of items per buffer" "4096"
blaar_add_option samplerate S "" "integer" "samplerate of the sound export" "44100"
blaar_add_parameter sound "blc_channel-in"  "sound todiffuse" 
blaar_parse_args "$@"

[ $filename ] && file_option="-f=$filename" || file_option=""

if [ $filename ]; then
    echo "to '$filename'" >&2
    [ $device ] && { echo "You cannot have file (-f $filename) and speaker (-D $device) at the same time"; exit 1; }
    blaar_module sndfile o_sndfile --file=$filename --size=$size --samplerate=$samplerate
elif [ $(uname) = "Darwin" ]; then #microphone
    echo "to coreaudio speaker" >&2
	blaar_module coreaudio o_coreaudio 
elif [ $(uname) = "Linux" ]; then
    echo "to ALSA speaker" >&2
	blaar_module asound o_asound
else 
	echo "$blaar_program_name does not work on '$(uname)' platform"
    exit 1
fi
