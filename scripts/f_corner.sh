source blaar.sh

blaar_init_options "Apply a f_filter on black and white camera and display it"
blaar_add_option gain g "" "real" "gain" "1"
blaar_add_option output o "" "blc_channel-out" "convolution output" :corner$$
blaar_add_option size s "" "(integer)x(interger)" "size of the filter" "3x3"
blaar_add_option grad_size G "" "(integer)x(interger)" "size of the gradient filter"
blaar_add_option max_size M "" "(integer)x(interger)" "size of the Max filter"
blaar_add_option sigma S "" "real" "sigma of the filter (-1 for size/6)" -1
blaar_add_parameter image "blc_channel-in" "1" "image you want to apply the gradient on"
blaar_parse_args "$@"

grad_size=${grad_size:-$size}
max_size=${max_size:-$size}

kernel=$(i_gaussian -D2 -s$max_size -o/kernel$$ -p0 2>$TMP_DIR/log ) || { cat $TMP_DIR/log; echo "Error creating the kernel"; exit 2; }

# pipe create a subshell therefore grad_norm is only visible by the subshel. That why we use {} to make all the thing using read in the same subshell.
# The read is needed to put the image before the kernel
f_grad_norm.sh -s$grad_size $image  |{
    read grad_norm
    f_conv -o$output $grad_norm $kernel
}
