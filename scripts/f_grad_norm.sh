source blaar.sh

blaar_init_options "Apply a f_filter on black and white camera and display it"
blaar_add_option gain g "" "real" "gain" "1"
blaar_add_option output o "" "blc_channel-out" "convolution output" :grad_norm$$
blaar_add_option size s "" "(integer)x(interger)" "size of the filter" "3x3"
blaar_add_option sigma S "" "real" "sigma of the filter (-1 for size/6)" -1
blaar_add_parameter image "blc_channel-in" "1" "image you want to apply the gradient on"
blaar_parse_args "$@"

f_grad.sh -pgrad.tsv -dx -g$gain -s$size $image |
{ read gradx
 f_grad.sh -pgrady.tsv -dy -g$gain -s$size -o:grady$$ $image |
 f_norm -o$output $gradx 
 
 #Cleaning intermediate blc_channels
 blc_channels -u$gradx
 blc_channels -u/grady$$
}
#read gradx ... needs to be in { } otherwise $gradx is not accessible by f_norm. After | read would have been in a subshell. With {} everything is in the same subshell. 


