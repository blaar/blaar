#!/usr/bin/env bash

# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but not defined
set -o pipefail #Error if a pipe fail

blaar_program_name=$(basename $0)

#Create a temporary directory, essencially for logs
TMP_DIR="$(mktemp -d -t ${blaar_program_name}XXX)"
trap 'error=$?; rm -rf "$TMP_DIR"; [ $error = 0 ] || { echo; echo "BLAAR: error $error executing $blaar_program_name $*"; echo; exit $error; }' EXIT INT HUP TERM ABRT

calc(){
	echo $(echo "scale=6; $@"|bc -l)
}

test_run() {
  "$@" || {
	  error=$?
    echo executing: "$@"
    exit $error
  }
}

blaar_module(){

  [ $# -lt 2 ] && { echo "You need at least 2 arguments but you have $#:'$*'"; usage; exit 1; }

  hash $2 2>/dev/null || { echo; echo "Fail to execute '$*'" ; echo "You need to install https://framagit.org/blaar/$1"; echo; exit 2; }
  shift

  test_run "$@" 
}

blaar_init_options(){
   [ $# = 1 ] || { usage; echo "You need a program desciption line. But you do not have argument"; exit 1; }
   export blaar_options_nb=0
   export blaar_parameters_nb=0
   export blaar_program_desciption="$1"
   
}


blaar_add_option(){
  (( $# < 5 )) && { echo "Wrong arguments number with add_option. You use '$#' instead of 5 or 6 with '$*'"; exit 1; }
  (( $# > 6 )) && { echo "Wrong arguments number with add_option. You use '$#' instead of 5 or 6 with '$*'"; exit 1; }
  [ $3 ] && { echo "Longname not yet in implemented. Use \"\" instead of '$3'"; exit 1; }

  local index=$blaar_options_nb

  blaar_options_variable[$index]="$1"
  blaar_options_letter[$index]="$2"
  blaar_options_longname[$index]="$3"
  blaar_options_parameter[$index]="$4"
  blaar_options_help[$index]="$5"
  
  export ${blaar_options_variable[$index]}=""
  
  if [ $# = 6 ]; then
    blaar_options_default[$index]="$6"
  else
	blaar_options_default[$index]=""
  fi
  blaar_options_nb=$(( $blaar_options_nb + 1 ))
}


blaar_add_parameter(){
  (( $# < 4 )) && { echo "Wrong arguments number with add_parameter. You use '$#' instead of 4 or 5 with '$*'"; exit 1; }
  (( $# > 5 )) && { echo "Wrong arguments number with add_parameter. You use '$#' instead of 4 or 5 with '$*'"; exit 1; }

  local index=$blaar_parameters_nb

  blaar_parameters_variable[$index]="$1"
  blaar_parameters_name[$index]="$2"
  blaar_parameters_required[$index]="$3"
  blaar_parameters_help[$index]="$4"  
  
  export ${blaar_parameters_variable[$index]}=""
  
  if [ $# = 5 ]; then
    blaar_parameters_default[$index]="$5"
  else
	blaar_parameters_default[$index]=""
  fi
  blaar_parameters_nb=$(( $blaar_parameters_nb + 1 ))
}

blaar_display_help(){
	echo
	[ $# = 0 ] || { echo "You should not have argument but you have '$*'"; exit 1; }
	printf "usage: $(basename $0)"
    for ((i = 0; i < $blaar_options_nb; i++))
    do
        printf " [-${blaar_options_letter[$i]} ${blaar_options_parameter[$i]}]"
    done

    for ((i = 0; i < $blaar_parameters_nb; i++))
    do
        if (( ${blaar_parameters_required[$i]} > 0 ))
        then
        for ((j = 0; j<${blaar_parameters_required[$i]}; j++ ))
        do
            printf " ${blaar_parameters_name[$i]}"
        done
        elif (( ${blaar_parameters_required[$i]} == 0 ))
        then
            printf " [ ${blaar_parameters_name[$i]} ]"
        else
            printf " [ ${blaar_parameters_name[$i]} ...]"
        fi
    done
    echo
	echo
	echo $blaar_program_desciption
    echo
    if (( $blaar_parameters_nb != 0))
    then
        echo "positional arguments:"
        for ((i = 0; i < $blaar_parameters_nb; i++))
        do
            if (( ${blaar_parameters_required[$i]} > 0 ))
            then
                for ((j = 0; j<${blaar_parameters_required[$i]}; j++ ))
                do
                    echo "  ${blaar_parameters_name[$i]}          ${blaar_parameters_help[$i]} "
            done
            elif (( ${blaar_parameters_required[$i]} == 0 ))
            then
                echo "  [ ${blaar_parameters_name[$i]} ]          ${blaar_parameters_help[$i]}"
            else
                echo "  [ ${blaar_parameters_name[$i]} ...]          ${blaar_parameters_help[$i]}"
            fi
        done
    fi
    echo
    if (( $blaar_options_nb != 0 ))
    then
        echo "optional arguments:"

        for ((i = 0; i < $blaar_options_nb; i++))
        do
		if [ ${blaar_options_default[$i]} ]; then
                default="(default: ${blaar_options_default[$i]})"
            else
                default=""
            fi
            echo " -${blaar_options_letter[$i]} ${blaar_options_parameter[$i]}          ${blaar_options_help[$i]} $default"
        done
    fi
}


blaar_parse_args(){
  #We say we need all default velue to remove them after when we read values
  for ((i = 0; i < $blaar_options_nb; i++))
  do
      default_needed_indexes[$i]=$i
  done

  blaar_add_option help "h" "" "" "display this help"
  optstring=""
  for ((i = 0; i < $blaar_options_nb; i++))
  do
    optstring="$optstring"${blaar_options_letter[$i]}
    [ "${blaar_options_parameter[$i]}" ] && optstring="$optstring:"
  done
  args=$(getopt $optstring "$@")
  set -- $args
  while (( $# > 0 ))
  do
    for ((i = 0; i < $blaar_options_nb; i++))
    do
	  [ $1 = "-h" ] && { blaar_display_help; exit 0; }
      if [ $1 = "-${blaar_options_letter[$i]}" ]; then
         if [ ${blaar_options_parameter[$i]} ]; then
             export ${blaar_options_variable[$i]}=$2
			 unset default_needed_indexes[$i]
             shift
         else
           ${blaar_options_variable[$i]}="1"
         fi
		 shift;
         break
      fi
      [[ $i = $blaar_options_nb ]] && { blaar_display_help; echo "Unexpected option '$1'"; return 1; }
    done
  	if [ $1 = "--" ];then
		shift 
		break
	fi
	
  done

#We test than 'default_needed_indexes' exists (not null) :- is to avoid error if default_needed_indexes is unbound .
if [ -n "${default_needed_indexes:-}" ]; then
  #We set default values
  for i in ${default_needed_indexes[@]}
  do
		export ${blaar_options_variable[$i]}="${blaar_options_default[$i]}"
  done
  
  for ((i = 0; i < $blaar_parameters_nb; i++))
  do
	  if (( ${blaar_parameters_required[$i]} > $# ))
	  then  
		  [ -t 0 ] &&echo "${blaar_parameters_name[$i]}     ${blaar_parameters_help[$i]} ?"
		  read ${blaar_parameters_variable[$i]}
		  export ${blaar_parameters_variable[$i]}
	  else
	  	export ${blaar_parameters_variable[$i]}=$1
  	  	shift
	fi
  done
fi
  
  return 0
}


