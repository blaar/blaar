source blaar.sh

blaar_init_options "Apply a gradient on input grey image"
blaar_add_option direction d "" "x|y" "direction of the gradient"
blaar_add_option gain g "" "real" "gain" "1"
blaar_add_option output o "" "blc_channel-out" "convolution output" ":grad<pid>"
blaar_add_option size s "" "(integer)x(interger)" "size of the filter" "3x3"
blaar_add_option sigma S "" "real" "sigma of the filter" -1
blaar_add_option profile p "" "filename" "profile the script" ""
blaar_add_parameter image "blc_channel-in" "1" "input image"
blaar_parse_args "$@"

[ "$direction" = "x" ] && option="-x1" || { [ "$direction" = "y" ] && option="-y1"; } || { echo "Wrong direction option '$direction'"; exit 1;  }

echo "Profile:$profile" >&2
[ "$profile" != "" ] && profile_option="-~$profile" || profile_option=""

kernel=$(i_gaussian $option -g$gain -s$size  -S$sigma -o/d${direction}gaussian$$ -p0 2>$TMP_DIR/log ) || { cat $TMP_DIR/log; echo "Error creating the kernel"; exit 2; }
# -p0 is because we do not want block

#./run.sh ipp/
f_ipp_convolute $profile_option -o$output  $image $kernel

#./run.sh armadillo/f_arma_convolute -o$output -~grad.tsv  $image $kernel
#./run.sh bapps/f_conv -o$output -~grad.tsv  $image $kernel


#remove the used blc_channels
blc_channels --unlink /d${direction}gaussian$$
