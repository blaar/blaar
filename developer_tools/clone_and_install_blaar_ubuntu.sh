#!/usr/bin/env bash
# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

{ #Bracket to be sure the file is fully loaded before excuting
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

echo
echo "Standard blaar install for Ubuntu"
echo "================================="
echo 
echo "Check that there is no conflicting directories"
echo
[ -d blaar ] && { echo "blaar directory already exist. Remove it or change the current install directory."; exit 1; }
[ -d blaar_build ] && { echo "blaar_build directory already exist. Remove it or change the current install directory."; exit 1; }
echo
echo "Install required packages"
echo
sudo apt-get install git g++ cmake ninja-build doxygen libpng-dev libjpeg-dev gnuplot fftw3-dev libsndfile1-dev libasound2-dev  libv4l-dev libgtk-3-dev #Only git g++ cmake ninja-build are really required
echo
echo "Download sources"
echo
git clone https://framagit.org/blaar/blaar
echo
cd blaar
echo
developer_tools/clone_standard_modules.sh
echo
echo Clone specific modules
echo
git submodule add https://framagit.org/blaar/asound.git
git submodule add https://framagit.org/blaar/i_v4l2_camera.git
echo
echo "Install and check each project"
echo
./install.sh all 
./check_all.sh
echo
echo "Update your bash profile"
echo
developer_tools/update_bash_profile.sh ~/.bashrc
echo
echo "    Success installing standard **blaar**"
echo
}


