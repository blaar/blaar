# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#Go in blaar directory
cd $(dirname $0)/..

source scripts/blaar.sh

#blc_core has to be first. It is used by all others projects
#blc_program has to be before blc_processes (  not nice dependecy)
lib_website=https://framagit.org/blibs
module_website=https://framagit.org/blaar

basic_libs="blc_core blc_channel blc_network blc_program blc_process blc_image"
standard_modules="blc_channels bapps gnuplot fftw sndfile png gtk"

# We do not use function in purpose as 'set -o errexit' has no effect in it
for lib in $basic_libs
do
echo "Cloning '$lib':"
	commandline="git submodule add $lib_website/$lib.git blibs/$lib"
	$commandline &> $TMP_DIR/clone_$lib.log || { echo "Fail cloning '$lib' executing: '$commandline'"; cat  $TMP_DIR/clone_$lib.log;  echo ; }
done

for module in $standard_modules
do
	echo "Cloning '$module':"
	commandline="git submodule add $module_website/$module.git"
	$commandline &> $TMP_DIR/clone_$module.log || { echo "Fail cloning '$module' executing: '$commandline'"; cat $TMP_DIR/clone_$module.log;  echo ; }
done
