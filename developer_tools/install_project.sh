# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

usage(){
    echo "Usage: install [-h] <project_directory> <Release|Debug|RelWithDebugInfo|MinSizeRel>"
    echo "    -h                      :Display this help."
    echo
}

args=$(getopt h $*) || { usage;  exit 1; }
set -- $args #update positional parameter
while [ $# -gt 0 ]
do
    case "$1" in
        (-h) usage; exit 0;;
        (--) shift; break;;
        (-*) echo "$0: error - unrecognized option $1" ; exit 1;;
        (*)  break;;
    esac
shift
done

[ $# -ne 2 ] && { echo "$0: You need 2 arguments. You have '$@'"; usage; exit 1;}

#We go to blaar directory save it and go to the previous directory. Usefull if install.sh is not call from blaar directory
cd $(dirname $0)/..
blaar_dir="$PWD"
cd - > /dev/null # goes back without messsage

build_type=$2

#We get the directory of the project
cd $1
project_dir="$PWD"
build_type=$2
project_name=`basename $project_dir`  #Name of the project is name of the directory
blaar_build_dir="${blaar_dir}_build"
build_dir="${blaar_build_dir}/cmake_files_${build_type}/${project_name}"

echo
echo " Install $1"
echo "==============="

cd $blaar_dir
./compile.sh $project_dir $build_type

cd $build_dir

#We check if there is an install target if Yes we install it.
if ninja -t targets all | grep "install: phony" ; then
    sudo ninja install
else
    echo "Nothing to install"
fi

