# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

#Bracket to be sure the file is fully loaded before excuting

echo
echo "Standard blaar install on Mac OSX"
echo "================================="
echo
echo "Check that there is no conflicting directories" 
[ -d blaar ] && { echo "blaar directory already exist. Remove it or change the current install directory."; exit 1; }
[ -d blaar_build ] && { echo "blaar_build directory already exist. Remove it or change the current install directory."; exit 1; }
echo
echo "Install standard packages"
echo 
hash brew  || {
  echo "Install the package manager homebrew (http://brew.sh)"
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}
echo
# if install fail it may be because it needs upgrade
brew install git cmake ninja doxygen libpng libjpeg gnuplot --with-qt fftw libsndfile || brew upgrade git cmake ninja doxygen libpng libjpeg gnuplot --with-qt fftw libsndfile #note:only git, cmake and ninja are really required
echo 
echo "Download blaar sources"
echo
git clone https://framagit.org/blaar/blaar
echo
cd blaar
echo
developer_tools/clone_standard_modules.sh 
echo
echo "Clone specific modules"
echo
git submodule add https://framagit.org/blaar/coreaudio.git
git submodule add https://framagit.org/blaar/i_AV_camera.git

echo
echo "Install and test each module"
echo
./install.sh all 
./check_all.sh
echo
echo "Update your .bash_profile"
echo
developer_tools/update_bash_profile.sh ~/.bash_profile
echo
echo "    Success installing standard **blaar**"
echo
}


