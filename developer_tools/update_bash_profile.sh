# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

# Check the existence and content of env variables PATH and BLAAR_DIR. Update ~/.bash_profile if required.

#We go in blaar dir
cd $(dirname $0)/..

[ $# = 1 ] && FILE=$1 || { echo "You need to specify a filename as first argument (i.e. ~/.bash_profile or ~/.bashrc)"; exit 1; }

if [  "${BLAAR_DIR+set}" ]; then  #Test if BLAAR_DIR is defined. "${BLAAR_DIR+set}" means contain "set" if exist
	if [ ! $BLAAR_DIR = "$PWD" ]; then
		#We update  reference to BLAAR_DIR in bash_profile
	 	sed -i~ -e"s|export BLAAR_DIR=$BLAAR_DIR|export BLAAR_DIR=$PWD|" $FILE
		echo
		echo "You had a previous installation of blaar in '$BLAAR_DIR'. We have redefined your standard version to '$PWD'"
		echo
		#Reinitialise value of PATH and BLAAR_DIR 
		echo "Restart your bash or EXECUTE:"
		echo "    PATH=\$(getconf PATH); source $FILE"
		echo
	fi
else 
	echo "We create or update your '$FILE' redefining env variables 'BLAAR_DIR' and 'PATH'"
	{	
	    echo
	    echo "#Definitions for blaar (http://blaar.org)"
		echo "export BLAAR_DIR=$PWD"
		echo 'export PATH=$PATH:$BLAAR_DIR/bin:$BLAAR_DIR/scripts'
		echo 
	} >> $FILE
	echo 
	echo "Restart your bash or execute:"
	echo "    source $FILE"
	echo
fi
