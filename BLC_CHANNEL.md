
Integration with blc_loop
=========================

output channel
--------------

Example defining a synchronous (':', '/' for asynchronous) channel of unsigned char ('UIN8') of 4096 values with no specific format ('NDEF').

    #include "blc_channel.h"
    #include "blc_program.h"
    
    ...
    
    blc_channel output;

    output.create_or_open(":toto", BLC_CHANNEL_WRITE, 'UIN8', 'NDEF', 1, 4096);
    output.publish();
    //Synchronize the BLC_COMMAND_LOOP with the output channel
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);

input channel
-------------

Open this same channel in reading synchronous (':', '/' for asynchronous) mode.

    #include "blc_channel.h"
    #include "blc_program.h"
    
    ...
    
    blc_channel input;

    input.open(":toto", BLC_CHANNEL_READ);
    //Synchronize the BLC_COMMAND_LOOP with the output channel
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);

