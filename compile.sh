#Basic Librairies And Applications for Robotics (BLAAR)
#Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2015 - 2017)
#This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
#You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
#In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
#Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
#The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but not defined
set -o pipefail #Error if a pipe fail

usage(){
    echo "usage: compile.sh [-h] <project dir> [Release|Debug|RelWithDebugInfo|MinSizeRel] [<target>]"
}

args=$(getopt h $*) || { usage;  exit 1; }
set -- $args #update positional parameter
while [ $# -gt 0 ]
do
case "$1" in
(-h) usage; exit 0;;
(--) shift; break;;
(-*) echo "$0: error - unrecognized option $1"; exit 1;;
(*)  break;;
esac
shift
done

[ $# -lt 1 ] && { echo "Missing project directory."; usage; exit 1; }

#We go to the project directory and get the directory name then we go back to seach for the baar directory
cd $1
project_dir="$PWD"
project_name=$(basename $project_dir)
cd - >/dev/null #Now we have the project directory we go back in current directory without message (/dev/null)

cd $(dirname $0) #We go in the directory of the current program which is in blaar
blaar_dir="$PWD"
blaar_build_dir="${blaar_dir}_build"

echo "Compiling **$project_dir**"

if [ $# -gt 1 ]; then
case $2 in
    Release|Debug|RelWithDebugInfo|MinSizeRel) build_type="$2";;
    *)echo "Unknown build type '$2'"; usage;;
esac
else build_type="Release"
fi

if [ $# -gt 3 ]; then
target=$(basename $3)
else
target="$project_name"
fi

build_dir="$blaar_build_dir/cmake_files_${build_type}/$project_name"
mkdir -p "$build_dir"
mkdir -p "$build_dir/lib"

mkdir -p "$blaar_build_dir/Release"

[ -d build ] || ln -s "$blaar_build_dir" build
[ -d bin ] || ln -s "$blaar_build_dir/Release" bin

cd $build_dir

#On mac and linux  -nt does not return the same if the file does not exist. We have to check the no existance or seniority
[ ! -e build.ninja -o $project_dir/CMakeLists.txt -nt build.ninja ] && {
    cmake --no-warn-unused-cli -GNinja    $project_dir -DCMAKE_BUILD_TYPE=$build_type -DLIBRARY_OUTPUT_PATH="${blaar_build_dir}/${build_type}/lib" -DEXECUTABLE_OUTPUT_PATH="${blaar_build_dir}/${build_type}"
}
ninja
