
Useful commands for git
=======================

Merge
=====

`git merge <the branch you merge from>`

Note that git pull merge from he repository branch

Conflicts:
----------
You can use **meld** to have a graphical interface to select the differences. 

You want to get only their modifications: `git checkout  --theirs <files>` 

You want to get only yours modifications: ` git checkout  --ours <files>`

Then `git add <files>`to say you fix the conflict.

Remove a specific commits: `git revert <commit id>`


Submodule
=========

Remove
------

    git submodule deinit -f -- <submodule>
    rm -rf .git/modules/<submodule>
    git rm -f <submodule>


