# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

usage(){
    echo
    echo "Usage: install [-h] [-v verbose] <all>|<project_directory> [Release|Debug|RelWithDebugInfo|MinSizeRel]"
    echo "    -h                      :Display this help."
    echo
}

VERBOSE=0

args=$(getopt hv $*) || { usage;  exit 1; }
set -- $args #update positional parameter
while [ $# -gt 0 ]
do
    case "$1" in
    (-h) usage; exit 0;;
    (-v) echo "Verbose mode"; VERBOSE=1;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" ; exit 1;;
    (*)  break;;
    esac
    shift
done

[ $# -lt 1 ] && { echo "You need at least an argument"; usage; exit 1;}

if [ $# -gt 1 ]; then
    case "$2" in
        Release|Debug|RelWithDebugInfo|MinSizeRel) build_type="$2";;
        *)usage; echo "Unknown build type '$2'"; exit 1;;
    esac
else
    build_type="Release"
fi


#We go to blaar directory save it and go to the previous directory. Usefull if install.sh is not call from blaar directory
cd $(dirname $0)

if [ "$1" = "all" ]; then
    echo
    echo "Install all current projects"
    echo "============================"
    echo
    mkdir -p /tmp/blaar
    #We extract porjects path from .gitmodules and apply install.sh on them
    cat .gitmodules | sed -n 's/.*path = //p' |
    while read project_path
    do
    project_name=`basename $project_path`
    command="developer_tools/install_project.sh $project_path $build_type"
    if [ $VERBOSE = 1 ]; then
        $command
    else
        echo "Install **$project_path**"
        $command > /tmp/blaar/install_${project_name}.log || 
        {
            echo; echo "Fail executing '$command'";
	    cat /tmp/blaar/install_${project_name}.log; echo; exit 2; };
    fi
    done
    echo
else
    developer_tools/install_project.sh $1 $build_type
fi

