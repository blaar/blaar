#!/bin/sh
#BLARR - Basic Librairies And Applications for Robotics
#Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2015 - 2016
#This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
#You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
#In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
#Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
#The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
#
#Author:  Arnaud Blanchard
#Date: April 2015

usage(){
echo "usage: valgrind.sh <project dir> [args ...]" >&2
}


[ $1 ] || { echo "Missing project directory."; usage; exit 1; }
cd $1;
project_dir="$PWD";
project_name=`basename $project_dir`;
cd - >/dev/null

cd `dirname $0`
blaar_dir="$PWD"
blaar_build_dir="${blaar_dir}_build"

./compile.sh "$project_dir" "Debug" "$project_name" >&2
[ $? ] || { echo "Fail compiling '$project_dir'" >&2; exit 2; }

bin_dir="$blaar_build_dir/Debug"

shift   #remove $0 i.e. run.sh
echo >&2
echo "Execute $project_name" >&2
echo "================" >&2
set -x #display command line

command -v valgrind &&{ set -x; valgrind --dsymutil=yes --leak-check=full --show-leak-kinds=all "$bin_dir/$project_name" "$@" || echo "Problems of memory"; } || { echo "You need to install valgrind"; exit 1; }



