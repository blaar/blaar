Install blaar on Ubuntu
=======================

Copy past this line in a terminal

    wget -q  -O/tmp/blaar_install.sh  https://framagit.org/blaar/blaar/raw/master/developer_tools/clone_and_install_blaar_ubuntu.sh &&
    bash /tmp/blaar_install.sh; rm /tmp/blaar_install.sh; cd blaar
    

You will be requested at some point to enter your admin password to make the installation.

You can do [**step by step**](https://framagit.org/blaar/blaar/blob/master/developer_tools/clone_and_install_blaar_ubuntu.sh) copying command line by command line. This may be useful to better understand and in case where the installer crash.
 
